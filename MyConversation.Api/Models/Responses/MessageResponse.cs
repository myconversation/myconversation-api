﻿namespace MyConversation.Api.Models.Responses
{
    public class MessageResponse
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public int ConversationId { get; set; }
    }
}
