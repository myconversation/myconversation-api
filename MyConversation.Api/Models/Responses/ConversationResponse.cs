﻿namespace MyConversation.Api.Models.Responses
{
    public class ConversationResponse
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
