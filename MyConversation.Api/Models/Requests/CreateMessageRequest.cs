﻿namespace MyConversation.Api.Models.Requests
{
    public class CreateMessageRequest
    {
        public string? Text { get; set; }
        public int ConversationId { get; set; }
    }
}
