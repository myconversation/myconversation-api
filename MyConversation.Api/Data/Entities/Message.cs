﻿namespace MyConversation.Api.Data.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public int ConversationId { get; set; }
    }
}
