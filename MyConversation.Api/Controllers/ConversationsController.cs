﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyConversation.Api.Data;
using MyConversation.Api.Data.Entities;
using MyConversation.Api.Models.Requests;
using MyConversation.Api.Models.Responses;

namespace MyConversation.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConversationsController : ControllerBase
    {
        private readonly MyDatabaseContext _context;

        public ConversationsController(MyDatabaseContext context)
        {
            _context = context;
        }

        #region Conversations
        // GET: api/Conversations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConversationResponse>>> GetConversations()
        {
            return await _context.Conversations
                .OrderByDescending(x => x.CreatedDate)
                .Select(x => ConvertToResponse(x))
                .ToListAsync();
        }

        // GET: api/Conversations/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<ConversationResponse>> GetConversation(int id)
        {
            var conversation = await _context.Conversations.FindAsync(id);
            if (conversation == null)
            {
                return NotFound();
            }

            return ConvertToResponse(conversation);
        }

        // POST: api/Conversations
        [HttpPost]
        public async Task<ActionResult<ConversationResponse>> PostConversation(CreateConversationRequest conversationRequest)
        {
            var conversationEntity = new Conversation
            {
                Title = conversationRequest.Title,
                CreatedDate = DateTimeOffset.UtcNow
            };

            _context.Conversations.Add(conversationEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetConversation), new { id = conversationEntity.Id }, ConvertToResponse(conversationEntity));
        }

        // PUT: api/Conversations/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConversation(int id, Conversation conversation)
        {
            if (id != conversation.Id)
            {
                return BadRequest();
            }

            var conversationEntity = await _context.Conversations.FindAsync(id);
            if (conversationEntity == null)
            {
                return NotFound();
            }

            conversationEntity.Title = conversation.Title;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!ConversationExists(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE: api/Conversations/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConversation(int id)
        {
            var conversation = await _context.Conversations.FindAsync(id);
            if (conversation == null)
            {
                return NotFound();
            }

            _context.Conversations.Remove(conversation);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        #endregion

        #region Conversation Messages
        // GET: api/Conversations/{id}/Messages
        [HttpGet("{id}/Messages")]
        public async Task<ActionResult<IEnumerable<MessageResponse>>> GetConversationMessages(int id)
        {
            return await _context.Messages
                .Where(x => x.ConversationId == id)
                .Select(x => ConvertToResponse(x))
                .ToListAsync();
        }

        // GET: api/Conversations/{id}/Messages/{messageId}
        [HttpGet("{id}/Messages/{messageId}")]
        public async Task<ActionResult<MessageResponse>> GetConversationMessage(int id, int messageId)
        {
            var message = await _context.Messages.FindAsync(messageId);
            if (message == null || message.ConversationId != id)
            {
                return NotFound();
            }

            return ConvertToResponse(message);
        }

        // POST: api/Conversations/{id}/Messages
        [HttpPost("{id}/Messages")]
        public async Task<ActionResult<MessageResponse>> PostConversationMessage(int id, CreateMessageRequest messageRequest)
        {
            if (id != messageRequest.ConversationId)
            {
                return BadRequest();
            }

            var conversationEntity = await _context.Conversations.FindAsync(id);
            if (conversationEntity == null)
            {
                return NotFound();
            }

            var messageEntity = new Message
            {
                Text = messageRequest.Text,
                ConversationId = messageRequest.ConversationId
            };

            conversationEntity.Messages.Add(messageEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetConversationMessage), new { id = messageEntity.ConversationId, messageId = messageEntity.Id }, ConvertToResponse(messageEntity));
        }
        #endregion

        #region Helpers
        private bool ConversationExists(int id)
        {
            return (_context.Conversations?.Any(x => x.Id == id)).GetValueOrDefault();
        }

        private bool MessageExists(int id)
        {
            return (_context.Messages?.Any(x => x.Id == id)).GetValueOrDefault();
        }

        private static ConversationResponse ConvertToResponse(Conversation conversation)
        {
            return new ConversationResponse
            {
                Id = conversation.Id,
                Title = conversation.Title,
                CreatedDate = conversation.CreatedDate
            };
        }

        private static MessageResponse ConvertToResponse(Message message)
        {
            return new MessageResponse
            {
                Id = message.Id,
                Text = message.Text,
                ConversationId = message.ConversationId
            };
        }
        #endregion
    }
}
